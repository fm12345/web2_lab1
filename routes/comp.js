const express = require('express');
const router = express.Router();

const database = require('../db');
const helper = require('./utils/compHelper.js')


router.get('/:random', async (req, res) => {
    let canEditScores = false
    if(req.oidc.isAuthenticated()){
        let link =  req.protocol + '://' + req.get('host') + req.originalUrl
        let comp_id = (await database.query(`SELECT id_natjecanje FROM natjecanje WHERE poveznica= ($1)`, [link])).rows[0].id_natjecanje
        let auth0_sub = (await database.query(`SELECT auth0_sub FROM korisnik NATURAL JOIN natjecanje WHERE id_natjecanje = ($1)`, [comp_id])).rows[0].auth0_sub
        //console.log(auth0_sub)
        //console.log(req.oidc.user.sub)
        canEditScores = (auth0_sub == req.oidc.user.sub) ? true : false
    }
    
    let rounds = (await helper.getRounds(req))
    let standings = (await helper.getStandings(req))

    res.render('comp', {
        compName: rounds[0].naziv, 
        canEditScores: canEditScores,
        user: req.oidc.user,
        rounds: rounds,
        standings: standings
        
    })
})



router.post('/:random', async (req, res) => {
    console.log(req.oidc.isAuthenticated())

    helper.saveResult(req,req.params.random)
    //console.log("--------------",req.body, req.params.random)
    //let rounds = (await helper.getRounds(req,req.params.random))

    let  uri =  req.params.random
    res.redirect(uri);
})

module.exports = router;
