const express = require('express');
const router = express.Router();

const database = require('../db');
const helper = require('./utils/indexHelper')



router.get('/', async (req, res) => {
    console.log(req.oidc.isAuthenticated())
    
    my_competitions = []
    if(req.oidc.isAuthenticated()){
        my_competitions = (await database.query(`SELECT naziv,poveznica FROM natjecanje JOIN korisnik ON natjecanje.auth0_sub = korisnik.auth0_sub 
                        WHERE korisnik.auth0_sub = $1`, [req.oidc.user.sub])).rows
    }
    

    res.render('index', { 
        isAuthenticated: req.oidc.isAuthenticated(),
        user: req.oidc.user,
        my_competitions : my_competitions
        })
})

router.post('/',async (req,res) =>{
    console.log(req.oidc.user)

    //save user to db
    rowCount = (await database.query(`SELECT * FROM Korisnik WHERE auth0_sub=$1`, [req.oidc.user.sub])).rowCount
    //console.log("broj redova " + rowCount)
    if(rowCount == 0){
        await database.query(`INSERT INTO korisnik (auth0_sub,email) VALUES  ($1, $2)`, [req.oidc.user.sub, req.oidc.user.email])
    }
    
    await helper.createCompetition(req)


    res.redirect("/")
})

module.exports = router;