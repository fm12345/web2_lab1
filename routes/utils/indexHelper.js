const database = require('../../db/index');


function generateRoundRobinSchedule(players) {
    let numPlayers = players.length;
    if(numPlayers == 2) return [players]
    if(numPlayers % 2 != 0){
        players.push("-")
        numPlayers++
    }

    const numRounds = numPlayers - 1;
    const schedule = [];
  
    for (let round = 1; round <= numRounds; round++) {
      const roundMatches = [];
  
      for (let i = 0; i < numPlayers - 1; i+=2) {
        let match =  [players[i], players[i + 1]]
        roundMatches.push(match);
      }
  
      schedule.push(roundMatches);
  
      // Rotiraj igrače osim prvog
      let firstPlayer = players.shift() // makni prvog
      let lastPlayer = players.pop() // makni zadnjeg
      players = [firstPlayer,lastPlayer].concat(players)
      
    }
  
    return schedule;
  }
  
  

async function createCompetition(req) {
    try {
        let competitors = req.body.competitors.split(";")
        if (competitors.length % 2 != 0){
            competitors.push("-")
        }
        let comp_name = req.body.competition_name
        let random = (Math.random() + 1).toString(36).substring(2);
        let link =  req.protocol + '://' + req.get('host') + req.originalUrl + 'natjecanja/'  + random;
        let sub = req.oidc.user.sub
        let num_of_comp = competitors.length
        let tmp = req.body.scoring_system.split("/")
        let win  = tmp[0]
        let draw = tmp[1]
        let lose = tmp[2]
    
    
        await database.query(`INSERT INTO natjecanje (naziv, poveznica, auth0_sub, broj_natjecatelja, bodovi_pobjeda, bodovi_remi, bodovi_poraz) 
                        VALUES ($1,$2,$3,$4,$5,$6,$7)`, [comp_name,link,sub,num_of_comp,win,draw,lose]);
    
        comp_id = (await database.query(`SELECT id_natjecanje FROM natjecanje WHERE naziv= ($1)`, [comp_name])).rows[0].id_natjecanje
        
        
        for(let competitor of competitors){
            await database.query(`INSERT INTO natjecatelj  (ime, natjecanje_id) VALUES ($1,$2)`,[competitor,comp_id])
        }

    
        let rounds = generateRoundRobinSchedule(competitors)
       
        for(let i = 1; i <= rounds.length; i++){
            (await database.query(`INSERT INTO kolo (redni_broj, natjecanje_id) VALUES ($1,$2)`, [i,comp_id])   )
            round_id = (await database.query(`SELECT * FROM kolo WHERE redni_broj = $1 AND natjecanje_id = $2`, [i, comp_id])  ).rows[0].id_kolo

            let currRound = rounds[i-1]
            //console.log("current round :" ,currRound)
            for(let j = 0; j < currRound.length; j++){
                // svako polje je jedan susret
                let match = currRound[j];
                let firstPlayer_id =   (await database.query(`SELECT id_natjecatelj FROM natjecatelj WHERE ime = $1 AND natjecanje_id = $2`, [match[0], comp_id])).rows[0].id_natjecatelj
                let secondPlayer_id =  (await database.query(`SELECT id_natjecatelj FROM natjecatelj WHERE ime = $1 AND natjecanje_id = $2`, [match[1], comp_id])).rows[0].id_natjecatelj


                
                await database.query(`INSERT INTO susret (domacin_id, gost_id , rezultat_domacin , rezultat_gost , id_kolo , odigran ) 
                    VALUES ($1,$2,$3,$4,$5,$6)`, [firstPlayer_id, secondPlayer_id, -1,-1,round_id, false])




            }
            
        }
        
    } catch (error) {
        console.log(error)
        
    }
 
    

}


module.exports = {generateRoundRobinSchedule, createCompetition}