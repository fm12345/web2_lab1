const database = require('../../db/index');



async function getRounds(req){
    let link =  req.protocol + '://' + req.get('host') + req.originalUrl
    let comp_id = (await database.query(`SELECT id_natjecanje FROM natjecanje WHERE poveznica= ($1)`, [link])).rows[0].id_natjecanje

    let rounds
    try {
        rounds = (await database.query(`SELECT poveznica,natjecanje.naziv, id_susret,redni_broj, domacin.ime AS domacin, gost.ime as gost, rezultat_domacin, rezultat_gost,odigran FROM 
        natjecanje JOIN KOLO on natjecanje.id_natjecanje = kolo.natjecanje_id JOIN susret ON kolo.id_kolo = susret.id_kolo
        JOIN natjecatelj AS domacin  ON domacin_id = domacin.id_natjecatelj JOIN natjecatelj AS gost ON gost.id_natjecatelj = gost_id
        WHERE natjecanje.id_natjecanje = ($1)
        ORDER BY redni_broj, domacin  `,[comp_id])).rows
    } catch (error) {
        
    }
    
    return rounds
    
}

async function saveResult(req, random){
    let link =  req.protocol + '://' + req.get('host') + req.originalUrl
    let comp_id = (await database.query(`SELECT id_natjecanje FROM natjecanje WHERE poveznica= ($1)`, [link])).rows[0].id_natjecanje

    let matchId = req.body.matchId;
    let homeScore = req.body.homeScore;
    let awayScore = req.body.awayScore;
    let isPlayed = true

    await database.query(`UPDATE susret
    SET rezultat_domacin = ($1) ,rezultat_gost = ($2), odigran = ($3)
    WHERE id_susret = ($4);`,[homeScore,awayScore, isPlayed,matchId])
    return;



}
  

async function getStandings(req){
    let link =  req.protocol + '://' + req.get('host') + req.originalUrl
    let comp = (await database.query(`SELECT * FROM natjecanje WHERE poveznica= ($1)`, [link])).rows[0]

    let competitors = (await database.query(`SELECT * FROM natjecatelj WHERE natjecanje_id = ($1)`,[comp.id_natjecanje])).rows
    competitors = competitors.filter(ojb => ojb.ime != '-')

    let result = []
    for(c of competitors){
        let homeGames = (await database.query(`SELECT * FROM susret WHERE domacin_id = ($1)`, [c.id_natjecatelj]) ).rows
        let homePoints = homeGames.map(obj => {
            if(obj.rezultat_domacin > obj.rezultat_gost) return comp.bodovi_pobjeda
            else if (obj.rezultat_domacin == obj.rezultat_gost && obj.rezultat_domacin != -1) return comp.bodovi_remi
            else return comp.bodovi_poraz
        })
        homePoints = homePoints.reduce((a,b) => a+b,0)


        let roadGames = (await database.query(`SELECT * FROM susret WHERE gost_id = ($1)`, [c.id_natjecatelj]) ).rows
        let roadPoints = roadGames.map(obj => {
            if(obj.rezultat_gost > obj.rezultat_domacin) return comp.bodovi_pobjeda
            else if (obj.rezultat_domacin == obj.rezultat_gost && obj.rezultat_domacin != -1) return comp.bodovi_remi
            else return comp.bodovi_poraz
        })

        roadPoints = roadPoints.reduce((a,b) => a+b,0)

        result.push({name: c.ime, points: homePoints + roadPoints})


    }

    result.sort((a,b) => b.points - a.points)
    return result

}


module.exports = { getRounds, saveResult, getStandings}