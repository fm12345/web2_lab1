const express = require('express');
var indexRouter = require('./routes/index.js')
var compRouter = require('./routes/comp.js')
const { auth } = require('express-openid-connect');
//var path = require('path');
require('dotenv').config()
    

const config = {
    authRequired: false,
    auth0Logout: true,
    secret: process.env.SECRET,
    baseURL: process.env.BASEURL,
    clientID: process.env.CLIENTID,
    issuerBaseURL: process.env.ISSUERBASEURL
  };
  


var app = express();
app.set('views', 'views');
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use(express.static('public')) // za statice fajlove
// auth router attaches /login, /logout, and /callback routes to the baseURL
app.use(auth(config));
app.use('/', indexRouter)
app.use('/natjecanja', compRouter)



// req.isAuthenticated is provided from the auth router
app.get('/', (req, res) => {
  
  res.send(req.oidc.isAuthenticated() ? 'Logged in' : 'Logged out');
});



app.listen(3000);
